---
title: "Monado - Building Additional Dependencies"
layout: main
---

* TOC
{:toc}

## Dependencies needed to build Monado's additional drivers

#### Preliminary notes:
These recipes have been checked on Ubuntu 22.04 LTS and Fedora 39.

Using the commands listed below, packages are installed in ```/usr/local```. Fedora, by default, does not search libraries or package config files in ```/usr/local```. You could adapt /etc/ld.so.conf, or change the commands to install in ```/usr```, or, more simply, you can set two envvars in your ```.bashrc```:
```sh
export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/lib64/pkgconfig
```

### For the OpenHMD driver

```sh
git clone https://github.com/OpenHMD/OpenHMD.git
cd OpenHMD
cmake -G Ninja -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local -DBUILD_SHARED_LIBS=ON
ninja -C build
sudo ninja -C build install
```

### For the libsurvive driver

Some other dependencies are needed.

* On Debian/Ubuntu:
  ```sh
  sudo apt install libopenblas-dev liblapacke-dev libopenvr-dev
  ```
* On Fedora:
  ```sh
  sudo dnf install openblas-devel lapack-devel openvr-devel
  ```

Then, libsurvive can be built and installed:
```sh
git clone https://github.com/cntools/libsurvive.git
cd libsurvive
cmake -G Ninja -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local
ninja -C build
sudo ninja -C build install
```

### For the Realsense driver

* On Debian/Ubuntu:
  ```sh
curl -sSf https://librealsense.intel.com/Debian/librealsense.pgp | sudo tee /etc/apt/keyrings/librealsense.pgp > /dev/null
echo "deb [signed-by=/etc/apt/keyrings/librealsense.pgp] https://librealsense.intel.com/Debian/apt-repo `lsb_release -cs` main" | \
sudo tee /etc/apt/sources.list.d/librealsense.list
sudo apt update
sudo apt install librealsense2-dkms
sudo apt install librealsense2-dev
```
* On Fedora:
  ```sh
  sudo dnf install librealsense-devel
  ```

### For the DepthAI driver

```sh
git clone --recurse-submodules https://github.com/luxonis/depthai-core.git
cd depthai-core
cmake -G Ninja -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local -DBUILD_SHARED_LIBS=ON
ninja -C build
sudo ninja -C build install
```

### For SLAM tracking support, using Basalt 

Some other dependencies are needed.

* On Debian/Ubuntu:
  ```sh
  sudo apt install libtbb-dev libglew-dev libpng-dev liblz4-dev libbz2-dev libboost-dev libboost-regex-dev libboost-filesystem-dev libboost-date-time-dev libboost-program-options-dev libboost-serialization-dev libgtest-dev libfmt-dev libuvc-dev
  ```
* On Fedora:
  ```sh
  sudo dnf install tbb-devel glew-devel libpng-devel lz4-devel bzip2-devel boost-devel boost-regex boost-filesystem boost-date-time boost-program-options boost-serialization gtest-devel fmt-devel libuvc-devel
  ```

Then, Basalt can be built and installed:
```sh
git clone --recurse-submodules https://gitlab.freedesktop.org/mateosss/basalt
cd basalt
cmake -G Ninja -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local
ninja -C build
sudo ninja -C build install
```

If you get `fatal error: Killed signal terminated program cc1plus` during the compilation, it could be due to memory exhaustion if too many compilation jobs are running in parallel. Using `ninja -j 1 -C build` to run one single compilation job should solve that issue.

**_Note for Fedora_**: Pangolin (in the included version) fails to compile with GCC 13, due to missing inclusions of ```<cstdint>``` (see https://www.gnu.org/software/gcc/gcc-13/porting_to.html, Header dependency changes).\
You need to add `#include <cstdint>` near the other inclusions in:

* `thirdparty/Pangolin/src/image/image_io_jpg.cpp`
* `thirdparty/Pangolin/include/pangolin/log/packetstream_tags.h`

### For 'Mercury' Hand Tracking, using the ONNX Runtime

**_Note for Ubuntu_**: ONNXRuntime needs a recent `cmake` (at least version 3.26), which is not available on Ubuntu 22.04. If you want to use the handtracking driver, then you'll have to update `cmake` using the Kitware's APT repository:
```sh
wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ jammy main' | sudo tee /etc/apt/sources.list.d/kitware.list >/dev/null
sudo apt update
sudo apt upgrade cmake
```

Some other dependencies are needed.

* On Debian/Ubuntu:
  ```sh
  sudo apt install libgmock-dev
  ```
* On Fedora:
  ```sh
  sudo dnf install gmock-devel
  ```

Then, onnxruntime can be built and installed:
```sh
git clone --recurse-submodules https://github.com/Microsoft/onnxruntime.git
cd onnxruntime
./build.sh --config Release --build_shared_lib --parallel --skip_tests --compile_no_warning_as_error --cmake_extra_defines CMAKE_INSTALL_PREFIX=/usr/local --cmake_extra_defines onnxruntime_BUILD_UNIT_TESTS=OFF
cd build/Linux/Release
sudo make install
```

### For the UltraLeap v5 driver

* On Debian/Ubuntu:
  ```sh
  wget -qO - https://repo.ultraleap.com/keys/apt/gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/ultraleap.gpg > /dev/null
  echo 'deb [arch=amd64] https://repo.ultraleap.com/apt stable main' | sudo tee /etc/apt/sources.list.d/ultraleap.list > /dev/null
  sudo apt update
  sudo apt install ultraleap-hand-tracking
  ```

* On Fedora:

  The Ultraleap v5 driver is not packaged for Fedora. A `repackager` script can be used to download the latest Gemini .deb packages and transform them into .rpm (to be adapted to the actual rpm files):

  ```sh
  sudo dnf install sed curl dpkg rpm-build
  
  git clone https://codeberg.org/pirvi/ultraleap-converter.git
  cd ultraleap-converter
  ./make_rpms
  
  sudo dnf install ./ultraleap-hand-tracking-service-5.17.1.0-1.fc39.x86_64.rpm
  sudo dnf install ./openxr-layer-ultraleap-1.6.5+2486adf9.CI1130164-1.fc39.x86_64.rpm
  sudo dnf install ./ultraleap-hand-tracking-control-panel-3.4.1-1.fc39.x86_64.rpm
  ```

### To use Tracy as a tracing backend

Some other dependencies are needed.

* On Debian/Ubuntu:
  ```sh
  sudo apt install libglfw3-dev libfreetype-dev libcapstone-dev libdbus-1-dev
  ```

* On Fedora:
  ```sh
  sudo dnf install glfw-devel freetype-devel capstone-devel dbus-devel
  ```

Then, tracy can be built:

**Note**: Currently (as of 03/21/2024), Monado uses the Tracy protocol version 63 (see ```src/external/tracy/common/TracyProtocol.hpp```), which needs Tracy version 0.9.0 or 0.9.1

```sh
git clone https://github.com/wolfpld/tracy.git
cd tracy
git checkout v0.9.1
cd profiler/build/unix
make LEGACY=1
```

This produces Tracy-release which can be copied in ```/usr/local/bin/``` for instance.

**Note**: Monado needs to be built with tracing support:

    cmake .... -DXRT_HAVE_TRACY=ON -DXRT_FEATURE_TRACING=ON

