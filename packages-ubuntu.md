---
title: "Monado - Ubuntu Packages"
layout: main
---

## PPA - for Focal (20.04)

We maintain a [Monado PPA](https://launchpad.net/~monado-xr/+archive/ubuntu/monado) with builds, currently for Ubuntu 20.04. The single PPA provides monado, the xr-hardware udev rules, and the OpenXR SDK (loader, layers, headers, sample apps).

To add the repository:

```sh
sudo add-apt-repository ppa:monado-xr/monado
sudo apt-get update
```

The packages are derived from the ones made for [Debian]({% link packages-debian.md %}).

## CI-built packages for Focal (20.04) and Jammy (22.04)

If you like living dangerously, you can install packages for Ubuntu Focal 20.04 LTS and Jammy 22.04 LTS directly from the latest master CI build with this additional repository:

### Ubuntu Focal 20.04

```sh
# Download CI signing key
sudo wget -nv https://monado.freedesktop.org/keys/monado-ci.asc -O /etc/apt/trusted.gpg.d/monado-ci.asc

# Add apt repo
echo 'deb https://monado.pages.freedesktop.org/monado/apt focal main' | sudo tee /etc/apt/sources.list.d/monado-ci.list

# Update package lists
sudo apt update
```

### Ubuntu Jammy 22.04

```sh
# Download CI signing key
sudo wget -nv https://monado.freedesktop.org/keys/monado-ci.asc -O /etc/apt/trusted.gpg.d/monado-ci.asc

# Add apt repo
echo 'deb https://monado.pages.freedesktop.org/monado/apt jammy main' | sudo tee /etc/apt/sources.list.d/monado-ci.list

# Update package lists
sudo apt update
```

For a description of the packages see [Getting Started]({% link
getting-started.md %}). Note that only the Monado packages are in this package
repo: you still will want to install the OpenXR loader and xr-hardware from
Ubuntu (or the PPA).
